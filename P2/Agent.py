# Agent.py

import sys
import Action
import numpy as np
from enum import Enum
import helper
import random
import json

class State:
    def __init__(self, world_map, current_position, looking_direction, reward_table):
        self.world_map = world_map
        self.current_position = current_position
        self.looking_direction = looking_direction
        self.reward_table = reward_table


class Agent:
    pending_actions = []
    pending_looking_direction = None

    def __init__(self):
        self.enviroment_rows = 4
        self.enviroment_columns = 4

        self.gamma = 0.8

        # Q_Value Table
        self.q_table = helper.get_init_q_table()

        # # Actions
        # self.actions = ['forward', 'left', 'right', 'grab', 'climb']

        # initial state
        self.Initialize()
    
    def __del__(self):
        pass
    
    def Initialize(self):
        self.current_state = State(world_map=helper.get_init_world_map(),
                                   current_position={'x': 0, 'y': 3},
                                   looking_direction='east',
                                   reward_table=helper.get_reward_table(helper.get_init_world_map()))
    
    def Process(self, percept):

        # pay the debt: we can't simply move north south west east, we must turn the looking direction first,
        # and that would cost movements
        if self.pending_actions:
            print("      ", self.pending_actions)
            return self.pending_actions.pop(0) # deque.popleft is faster, but this should be enough for this task

        if self.pending_looking_direction:
            self.current_state.looking_direction = self.pending_looking_direction
            self.pending_looking_direction = None



        possible_actions = self.get_possible_actions()
        next_action = random.choice(possible_actions)
        self.update_q_table(next_action)

        if next_action in ['north', 'south', 'west', 'east']:
            return self.go_direction(next_action)

        elif next_action == 'grab':
            x = self.current_state.current_position['x']
            y = self.current_state.current_position['y']

            self.current_state.world_map[y][x] = '_' # delete the gold tile

            if helper.is_map_cleared(self.current_state.world_map):
                self.current_state.world_map[0][3] = 'L' # attach the ladder into the map

            self.current_state.reward_table = helper.get_reward_table(self.current_state.world_map)
            return Action.GRAB

        else: # action == climb
            return Action.CLIMB


    def GameOver(self, score):
        q_json = json.dumps(self.q_table, indent=2)
        # print("STOOOOPEED")
        with open('q_table.json', 'w') as f:
            f.write(q_json)


    def update_q_table(self, next_action)->None:
        # print("      ", next_action)
        # print("      ", self.current_state.current_position)
        next_position = self.get_next_position(next_action)

        x = self.current_state.current_position['x']
        y = self.current_state.current_position['y']
        reward_table = self.current_state.reward_table

        next_x = next_position['x']
        next_y = next_position['y']

        q_predict = self.q_table[y][x][next_action]
        # print(f'q_predict:{q_predict}')
        # print(f'reward cell: {reward_table[y][x]}')
        q_target = reward_table[y][x][next_action] + \
                        self.gamma * max(self.q_table[next_y][next_x].values())

        # print("      ", self.q_table)
        # print("      ", self.q_table[y][x])
        self.q_table[y][x][next_action] += q_target - q_predict
        # print("      ", self.q_table)


    def get_max_q_actions(self)->list:
        x = self.current_state.current_position['x']
        y = self.current_state.current_position['y']

        rewards = self.current_state.reward_table[y][x]

        possible_actions = []
        max_q = -999999
        for action in rewards.keys():
            if rewards[action] is not None:
                possible_actions.append(action)

                max_q = max(max_q, self.q_table[y][x][action])

        # find all actions with maximum q
        max_reward_actions = []
        for action in possible_actions:
            if self.q_table[y][x][action] == max_q:
                max_reward_actions.append(action)

        return max_reward_actions


    def get_possible_actions(self)->list:
        x = self.current_state.current_position['x']
        y = self.current_state.current_position['y']

        rewards = self.current_state.reward_table[y][x]
        print(f"      current pos:x = {x}, y = {y}")

        possible_actions = []
        for action in rewards.keys():
            if rewards[action] is not None:
                print("      ", action)
                possible_actions.append(action)


        return possible_actions


    def get_next_position(self, next_action):
        next_position = self.current_state.current_position.copy() # just need to in/decrease x or y later
        if next_action == 'north':
            next_position['y'] -= 1
        elif next_action == 'south':
            next_position['y'] += 1
        elif next_action == 'west':
            next_position['x'] -= 1
        elif next_action == 'east':
            next_position['x'] += 1

        return next_position


    def go_direction(self, direction):
        # really, really should not use +=, but I'm a lazy ass so I use here anyways so that it would be shorter.

        looking_direction = self.current_state.looking_direction
        # next_position = self.current_state.current_position.copy() # just need to in/decrease x or y later

        if (direction == 'north'):
            self.current_state.current_position['y'] -= 1

            # go north, facing north
            if looking_direction == 'north':
                return Action.GOFORWARD
            # go north, facing south    
            elif looking_direction == 'south':
                self.pending_actions = [Action.TURNRIGHT, Action.TURNRIGHT, Action.GOFORWARD]
            # go north, facing west
            elif looking_direction == 'west':
                self.pending_actions = [Action.TURNRIGHT, Action.GOFORWARD]
            # go north, facing east
            else:
                self.pending_actions = [Action.TURNLEFT, Action.GOFORWARD]

        elif (direction == 'south'):
            self.current_state.current_position['y'] += 1

            # go south, facing north
            if looking_direction == 'north':
                self.pending_actions = [Action.TURNRIGHT, Action.TURNRIGHT, Action.GOFORWARD]
            # go south, facing south
            elif (looking_direction == 'south'):
                return Action.GOFORWARD
            # go south, facing west
            elif (looking_direction == 'west'):
                self.pending_actions = [Action.TURNLEFT, Action.GOFORWARD]
            # go south, facing east
            else:
                self.pending_actions = [Action.TURNRIGHT, Action.GOFORWARD]

        elif (direction == 'west'):
            self.current_state.current_position['x'] -= 1

            # go west, facing north
            if (looking_direction == 'north'):
                self.pending_actions = [Action.TURNLEFT, Action.GOFORWARD]
            # go west, facing south
            elif (looking_direction == 'south'):
                self.pending_actions = [Action.TURNRIGHT, Action.GOFORWARD]
            # go west, facing west
            elif (looking_direction == 'west'):
                return Action.GOFORWARD
            # go west, facing east
            else:
                self.pending_actions = [Action.TURNRIGHT, Action.TURNRIGHT, Action.GOFORWARD]

        elif (direction == 'east'):
            self.current_state.current_position['x'] += 1

            # go east, facing north
            if (looking_direction == 'north'):
                self.pending_actions = [Action.TURNRIGHT, Action.GOFORWARD]
            # go east, facing south
            elif (looking_direction == 'south'):
                self.pending_actions = [Action.TURNLEFT, Action.GOFORWARD]
            # go east, facing west
            elif (looking_direction == 'west'):
                self.pending_actions = [Action.TURNRIGHT, Action.TURNRIGHT, Action.GOFORWARD]
            # go east, facing east
            else:
                return Action.GOFORWARD

        self.pending_looking_direction = direction
        return self.pending_actions.pop(0)

    # Helper Functions
    # define a function that determines if the specified location is a terminal state

    # def is_terminal_state(self, current_y, current_x):
    #     if self.rewards[current_y, current_x] == -1.:
    #         return False
    #     else:
    #         return True

    # # define a epsilon greedy algorithm that will choose which action to take next

    # def get_next_action(self, current_y, current_x, epsilon):
    #     # for 0.8 choose best action
    #     if np.random.random() < epsilon:
    #         # TODO: Change this
    #         return np.argmax(self.q_values[current_y, current_x])
    #     # for 0.2 chose either 0.1 left or 0.1 right
    #     else:
    #         return np.random.randint(2)

    # # define a function that will get the next location based on the chosen action

    # def get_next_location(self, current_y, current_x, action_index):
    #     new_row_index = current_y
    #     new_column_index = current_x
    #     if self.actions[action_index] == 'up' and current_y > 0:
    #         new_row_index -= 1
    #     elif self.actions[action_index] == 'right' and current_x < self.enviroment_columns - 1:
    #         new_column_index += 1
    #     elif self.actions[action_index] == 'down' and current_y < self.enviroment_rows - 1:
    #         new_row_index += 1
    #     elif self.actions[action_index] == 'left' and current_x > 0:
    #         new_column_index -= 1
    #     return new_row_index, new_column_index

    # def get_shortest_path(self, start_row_index, start_column_index):
    #     if self.is_terminal_state(start_row_index, start_column_index):
    #         return []
    #     else:
    #         current_y, current_x = start_row_index, start_column_index
    #         shortest_path = [[current_y, current_x]]
    #         while not self.is_terminal_state(current_y, current_x):
    #             action_index = self.get_next_action(current_y, current_x, 1.)
    #             current_y, current_x = self.get_next_location(current_y, current_x,
    #                                                                         action_index)
    #             shortest_path.append([current_y, current_x])
    #         return shortest_path
