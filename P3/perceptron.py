import numpy as np
from numba import jit

class Perceptron():
    def __init__(self, weight_shape):
        self.synaptic_weights = np.random.random(weight_shape)
        print(f'Randomized weights:\n{self.synaptic_weights}\n')
        
    #sigmoid function
    @staticmethod
    # @jit(forceobj=True)
    def sigmoid(x):
        if hasattr(x, "__iter__"): # recurse if iterable
            return np.array(list(map(Perceptron.sigmoid, x))) # magic recursion 
        else:
            return 1 / (1 + np.exp(-x))

    #derivative of sigmoid function
    @staticmethod
    # @jit(forceobj=True)
    def sigmoid_derivative(x):
        sigmoid = Perceptron.sigmoid(x)
        return sigmoid * (1 - sigmoid)
    
    #training loop
    # @jit(forceobj=True)
    def train(self, inputs, targets, iterations):
        for _ in range(iterations):
            outputs = self.think(inputs)
            error_out = targets - outputs
            delta_weight = (error_out * Perceptron.sigmoid_derivative(outputs)).dot(inputs.T)
            self.synaptic_weights += delta_weight
    
    #one calculation step of the perceptron
    # @jit(forceobj=True)
    def think(self, inputs):
        return Perceptron.sigmoid(self.synaptic_weights.dot(inputs))
        
        
if __name__ == "__main__":
    print("A3.1:\n")
    input_set = np.array([[0, 0, 1],
                          [1, 1, 1],
                          [1, 0, 0],
                          [0, 1, 1]])
    targets = np.reshape([0, 1, 1, 0], (4,1))
    p = Perceptron(weight_shape=(1,3))

    for row, target in zip(input_set, targets):
        data = row.reshape(3,1)
        p.train(inputs=data, targets=target.reshape((1,1)), iterations=50)

    print(f'Trained weights: \n{p.synaptic_weights}\n')
    print("Prediction for [1 1 0]: ", p.think(np.array([1, 1, 0])))