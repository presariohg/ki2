import numpy as np
import pandas as pd
from perceptron import Perceptron
from numba import jit
import sys
import time
np.set_printoptions(threshold=sys.maxsize)

class NeuralNetwork():

    #initialize the neural network
    def __init__(self, input_nodes_count, hidden_nodes_count, output_nodes_count, learning_rate):
        self.weights_ih = np.random.random((hidden_nodes_count, input_nodes_count))
        self.weights_ho = np.random.random((output_nodes_count, hidden_nodes_count))
        self.learning_rate = learning_rate

    #train the neural network
    def train(self, inputs, targets):
        outputs, hiddens = self.think(inputs)
        error_out = targets - outputs  # shape: (output_nodes, 1)
        error_hidden = self.weights_ho.T.dot(error_out) # shape: (hidden_nodes, output_nodes)

        self.weights_ho += (error_out * Perceptron.sigmoid_derivative(outputs)).dot(hiddens.T) * (self.learning_rate)
        self.weights_ih += (error_hidden * Perceptron.sigmoid_derivative(hiddens)).dot(inputs.T) * (self.learning_rate)

    def think(self, inputs): # input shape: (input_nodes, 1)
        hiddens = Perceptron.sigmoid(self.weights_ih.dot(inputs)) # shape: (1, hidden_nodes)

        return Perceptron.sigmoid(self.weights_ho.dot(hiddens)), hiddens # shape: (output_nodes, 1)
        
if __name__ == "__main__":

    input_nodes = 784 #28*28 pixel
    hidden_nodes = 200 #voodoo magic number
    output_nodes = 10 #numbers from [0:9]

    learning_rate = 0.1 #feel free to play around with

    # train_set = pd.read_csv("mnist_train_100.csv", header=None)
    train_set = pd.read_csv("mnist_test_full.csv", header=None)
    test_set = pd.read_csv("mnist_train_100.csv", header=None)
    n = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)

    start = time.time()
    epoch_count = 2

    for epoch in range(epoch_count):
        print(f"\nEpoch: {epoch + 1}/{epoch_count}")
        for row in range(len(train_set)):
            if row % 500 == 0:
                print(f"This epoch {row * 100/len(train_set)}% done")

            label_raw = train_set.loc[row, 0]
            label = [0] * 10
            label[label_raw] = 1
            label = np.array(label).reshape((output_nodes, 1))

            data = np.array(train_set.loc[row, 1:]).reshape((input_nodes, 1))
            data_normalized = data / 255 

            n.train(inputs=data_normalized, targets=label)

    print(f"Enlapsed: {(time.time() - start):.2f}s")
    
    true_count = 0
    for row in range(len(test_set)):
    # for row in range(1):
        label = test_set.loc[row, 0]
        # label = train_set.loc[row, 0]

        data = np.array(test_set.loc[row, 1:]).reshape((input_nodes, 1))
        # data = np.array(train_set.loc[row, 1:]).reshape((input_nodes, 1))
        data_normalized = data / 255  

        result, _ = n.think(data_normalized)
        if (label.T == np.argmax(result)):
            true_count += 1

    print(f"Accuracy: {(true_count / len(test_set))}")


